let number;

number = prompt('Please enter a number');

while(number <= 1 || number != Math.round(number)) {
    number = prompt('Please enter a prime number');
}

function primeNumber(n) {
    let result = true;
        for (let i = 2; i <= Math.sqrt(n); i++){
            if(n % i == 0){
                result = false;
                break;
            }
        }
    return result;
}

for (let j = 1; j <= number; j++){
    let checker = primeNumber(j);
    if (checker === true)
    console.log(j);
}













